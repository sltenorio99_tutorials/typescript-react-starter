import React from 'react';
import { FormState } from '../types';

export class MyForm extends React.Component<{}, FormState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      full_name: "",
      age: null,
    }
  }

  defaultState = {
    full_name: ""
  }

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value }: { name: string, value: string } = e.target;

    // Enforce key name to be one of FormState
    const fieldName: keyof FormState = name as keyof FormState;

    // Workaround for compiler issues with https://github.com/DefinitelyTyped/DefinitelyTyped/issues/26635#issuecomment-400260278
    this.setState((prevState: FormState): FormState => ({
      ...prevState,
      [fieldName]: value
    }));
  }

  render() {
    
    const { full_name }: {full_name: string} = this.state;

    return (
      <form>
        <input name="full_name" value={full_name} onChange={this.handleChange}/>
      </form>
    )
  }
}